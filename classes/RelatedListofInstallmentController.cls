/**    Description    :    Apex class to show related Installment of Donation in Detail of Account.
  *
  *    Created By     :    Abhishek Tripathi (Extentor Tquila)    
  *
  *    Created Date   :    12/18/2014
  *
  *    Revisison Log  :    12/18/2014
  *
  *    Version        :    V1.0 Created 
  **/
public class RelatedListofInstallmentController {

    //Object instances
    public Contact con { get; set; }
    public List<Donation_Installment__c> installDonation { get; set; }
    
    //Constructor
    public RelatedListofInstallmentController (Apexpages.standardController stdController) {
    
        this.con = (Contact)stdController.getRecord();
        installDonation = [Select Id, Name, Amount__c, Close_Date__c, Is_Contribution__c, Overall__c, Due_Date__c, Earmarked_Funding__c, Status__c From Donation_Installment__c Where Related_Contact__c =: con.Id];    
    }

    //Method to open a new record
    public PageReference openNewIn() {
    
        Contact conName = [ Select Id, Name From Contact Where Id =: con.Id];
        return new PageReference('/apex/EditPageForDonationInstallments?CF00N9000000CzPte='+ conName.Name +'&CF00N9000000CzPte_lkid='+ conName.Id +'&scontrolCaching=1&retURL=%2F0039000001Jh2dq&sfdc.override=1');
    
    }
}