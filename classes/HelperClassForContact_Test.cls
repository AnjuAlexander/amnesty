/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class HelperClassForContact_Test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Map<String,schema.RecordTypeInfo> RTMapByName = Schema.SObjectType.Contact.getRecordTypeInfosByName();
		Schema.RecordTypeInfo individual = RTMapByName.get('Individual');
		Schema.RecordTypeInfo institutional = RTMapByName.get('Institutional');
		
		List<Contact> contacts = new List<Contact>();
		
        //Create Contact record for individuals 
        Contact con = new Contact();
        con.FirstName = 'Testing';
        con.LastName = 'Test';
        con.Email = 'test@testing.com';
        con.RecordTypeId = individual.getRecordTypeId();
        
        contacts.add(con);
        insert contacts;
        
        try {
        
        	//Test starts here
        	Test.startTest();
        
            //Create Contact record
	        Contact dCon = new Contact();
	        dCon.FirstName = 'Testing';
	        dCon.LastName = 'Test';
	        dCon.Email = 'test@testing.com';
	        dCon.RecordTypeId = individual.getRecordTypeId();
	        
	        insert dCon;
	        
	    	Test.stopTest();
        
        } catch( Exception e) {
        	
        	Boolean expectedExceptionThrown =  e.getMessage().contains('Email Id already Exist! Duplicate Email Id found') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);	
        }
    }
    
    static testMethod void myUnitTest2() {
    
    	Map<String,schema.RecordTypeInfo> RTMapByName = Schema.SObjectType.Contact.getRecordTypeInfosByName();
		Schema.RecordTypeInfo institutional = RTMapByName.get('Institutional');
		
    	//Create Contact record for institutionals
        Contact conv = new Contact();
        conv.FirstName = 'Amnesty';
        conv.LastName = 'Intern';
        conv.Email = 'test@testing.com';
        conv.RecordTypeId = institutional.getRecordTypeId();
       
       	insert conv;
       	
    	try {
        
       	 	//Test starts here
        	Test.startTest();
       
        	//Create Contact record
	        Contact cCon = new Contact();
	        cCon.FirstName = 'Amnesty';
	        cCon.LastName = 'Intern';
	        cCon.Email = 'test@testing.com';
	        cCon.RecordTypeId = institutional.getRecordTypeId();
	        
	    	insert cCon;
	    
	    	Test.stopTest();
        } catch( Exception e) {
        	
        	Boolean expectedExceptionThrown =  e.getMessage().contains('Name already Exist! Duplicate Name found') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);	
        }
       	
    }
}