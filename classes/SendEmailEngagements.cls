/**    Description    :    Apex class to show list view of Donors (List view of Contacts).
  *
  *    Created By     :    Abhishek Tripathi (Extentor Tquila)    
  *
  *    Created Date   :    12/16/2014
  *
  *    Revisison Log  :    12/16/2014
  *
  *    Version        :    V1.0 Created
  **/
public with sharing class SendEmailEngagements {

    //String Id
    String accId;
    public Id selectedTempalte { get; set; }
    List<String> emailIds ;
    
    public List<SelectOption> emailTemplates { get; set; }
    
    //Contructor
    public SendEmailEngagements() {
        
        //Initiallize
        emailTemplates = new List<SelectOption>();
        selectedTempalte = null;
        emailIds = new List<String>();
        accId = '';
        
        System.debug(':::::::::::::::::::::' + ApexPages.currentPage().getParameters().get('Id') );
        //fetch Account Id from the URL
        for(Account acc : [Select Id, Primary_Email__c, Secondary_Email__c From Account Where Id =: ApexPages.currentPage().getParameters().get('Id')]) {
            
            //Assign value
            accId = acc.Id;
            
            //Check for Email Ids
            if(acc.Primary_Email__c != '' ) {
                emailIds.add(acc.Primary_Email__c);
            }
            
            if(acc.Secondary_Email__c != '' && acc.Secondary_Email__c != null) {
                emailIds.add(acc.Secondary_Email__c );
            }
        }
        
        //Fetch email templates and populate select Options
        for(EmailTemplate em : [Select Id, Name, IsActive, Folder.Name From EmailTemplate ]) {
            emailTemplates.add(new SelectOption( em.Id, em.Name ));
        }
    }
    
    /**
    *   Description :   Send Email to using the template    
    *
    *   Return      :   void
    **/
    public void sendEmail() {
        
            // Define the email
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
            
            // Sets the paramaters of the email
            email.setToAddresses( emailIds );
            email.setTemplateId( selectedTempalte );  
            email.setTargetObjectId( UserInfo.getUserId());
            email.setSaveAsActivity(false);
            
            // Sends the email
            //Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
            List<Messaging.SendEmailResult> results = Messaging.sendEmail(new Messaging.Email[] { email });
            
            //Check if email sent
            if(results.get(0).isSuccess()) {
                
               /* //List of Engagements
                List<Engagements__c> angagemenets = new List<Engagements__c>();
                
                //Creating a new Record and Saving new Engagement
                Engagements__c eng = new Engagements__c();
                eng.Contact_Name__c = Id.valueOf(accId);
                eng.Date__c = date.today();
                eng.Initiator__c = UserInfo.getUserId();
                eng.Notes__c = '';
                eng.Type_of_Engagement__c = 'Email';
                
                //Add to list
                angagemenets.add(eng);
            
                //Check for size
                if(angagemenets.size() > 0) {
                    insert angagemenets;
                }
                */
            }
        
    }
}