/**    Description    :    Apex class to show list view of Donors (List view of Contacts).
  *
  *    Created By     :    Abhishek Tripathi (Extentor Tquila)    
  *
  *    Created Date   :    12/15/2014
  *
  *    Revisison Log  :    12/15/2014
  *
  *    Version        :    V1.0 Created
  **/
public with sharing class ContactListViewController {
    
    // Setting List view values
    public String listName {
        get;
        set {
            
            //List view Name
            listName = 'Donors';
            
            //Query the contacts
            String qry = 'SELECT Name FROM Contact';
        
            //Get the records
            ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(Database.getQueryLocator(qry));
        
            //Populating the Select Options list
            List<SelectOption> allViews = ssc.getListViewOptions();
        
            for (SelectOption so : allViews) {
           
                if (so.getLabel() == listName) {
                    // for some reason, won't work with 18 digit ID
                    listId = so.getValue().substring(0,15);
                    break;
                }
            }             
        }      
    }
    
    //String
    public String listId {get;set;}
}