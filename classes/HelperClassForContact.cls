/**    Description    :    Apex class work as the helper class for the trigger on 	Contact.
  *
  *    Created By     :    Abhishek Tripathi (Extentor Tquila)    
  *
  *    Created Date   :    12/22/2014
  *
  *    Revisison Log  :    12/22/2014
  *
  *    Version        :    V1.0 Created
  **/
public with sharing class HelperClassForContact {

	/**
	  *	Description	:	Restrict user to insert Duplicate records in Contact object
	  *
	  *	Return		:	Void
	  *
	  *	Parameter	:	List<Contact>
	 **/
	public static void RestricDuplicateRecords( List<Contact> newContacts ) {
		
		Map<String,schema.RecordTypeInfo> RTMapByName = Schema.SObjectType.Contact.getRecordTypeInfosByName();
		Schema.RecordTypeInfo individual = RTMapByName.get('Individual');
		Schema.RecordTypeInfo institutional = RTMapByName.get('Institutional');
		
		Map<String, List<Contact>> mapOfContactWithEmail = new Map<String, List<Contact>>();
		Map<String, List<Contact>> mapOfContactWithName = new Map<String, List<Contact>>();
		
		//Loop through newContact list
		for(Contact con : newContacts) {
			
			System.debug('con.RecordTypeId:::::::::' + con.RecordTypeId + individual.getRecordTypeId());
			
			//Check for RecordType
			if(con.RecordTypeId == individual.getRecordTypeId() && con.Email != null) {
				
				if(mapOfContactWithEmail.containsKey(con.Email)) {
					mapOfContactWithEmail.get(con.Email).add(con);
				} else {
					mapOfContactWithEmail.put(con.Email, new List<Contact>{con});
				}
			}
			
			//Check for institutional record type
			if(con.RecordTypeId == institutional.getRecordTypeId()) {
			
				System.debug('con.RecordTypeId:::::::::' + con.RecordTypeId +'and'+ con.Name + individual.getRecordTypeId());
				
				if(mapOfContactWithName.containsKey(con.FirstName +' '+con.LastName)) {
					mapOfContactWithName.get(con.FirstName +' '+con.LastName).add(con);
				} else {
					mapOfContactWithName.put(con.FirstName+' '+con.LastName, new List<Contact>{con});
				}
			}
		}
		
		System.debug('mapOfContactWithEmail:::::::::' + mapOfContactWithEmail);
			
		//Check for Map size
		if(mapOfContactWithEmail.size() > 0) {
		
			//Query through Contact records
			for(Contact exCon : [Select Id, Email From Contact Where Email =: mapOfContactWithEmail.keySet()]) {
				
				//Check for map key
				if(mapOfContactWithEmail.containsKey(exCon.Email)) {
				
					//Add error
					for(Contact dCon : mapOfContactWithEmail.get(exCon.Email)) {
						dCon.AddError('Email Id already Exist! Duplicate Email Id found');
					}
				}
			}
		}
		
		System.debug('mapOfContactWithName :::::::::' + mapOfContactWithName );
		
		//Check for map size
		if(mapOfContactWithName.size() > 0 ){
			
			//Query through Contact records
			for(Contact exCon : [Select Id, Name From Contact Where Name =: mapOfContactWithName.keySet()]) {
				
				//Check for map key
				if(mapOfContactWithName.containsKey(exCon.Name)) {
				
					//Add error
					for(Contact dCon : mapOfContactWithName.get(exCon.Name)) {
						dCon.AddError('Name already Exist! Duplicate Name found');
					}
				}
			}
		}
 	}
}