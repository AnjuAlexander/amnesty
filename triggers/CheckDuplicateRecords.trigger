/**    Description    :    Apex trigger on Contact Object.
  *
  *    Created By     :    Abhishek Tripathi (Extentor Tquila)    
  *
  *    Created Date   :    12/22/2014
  *
  *    Revisison Log  :    12/22/2014
  *
  *    Version        :    V1.0 Created
  **/
trigger CheckDuplicateRecords on Contact (before insert) {

	//Check for Events
	if(Trigger.isBefore) {
		
		//Check for DML events
		if(Trigger.isInsert) {
			
			//Calling helper class restrict user insert duplicate records
			HelperClassForContact.RestricDuplicateRecords(Trigger.new);
		}
	}
}